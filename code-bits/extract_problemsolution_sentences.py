import csv
import re

original_input = 'db_sentences_len20.csv'
problem_solution_output = 'problem_solution_sentence_extractions.csv' #specific to your sentence relationship

def main():
    filter_problem_solution(original_input)

def filter_problem_solution(filename):
    # open input data file

    with open(filename, 'r', newline='') as input_file:
        # prepare to read the rows of the file using the csv packages' DictReader routines
        sentence_reader = csv.DictReader(input_file, delimiter=',', quotechar='"')
        #open a new output file
        with open(problem_solution_output, 'w', newline='') as output_file:
            # keep all columns data and add CrowdFlower_Choices for future task
            sentence_writer = csv.DictWriter(output_file,
                                                 fieldnames = ['idSentence', 'Text', 'idCorpus','TargetWord', 'Length',
                                                               'ReadingLevel','GrammarReadingLevel_SMO',
                                                               'CrowdFlower_Choices'],
                                                 extrasaction = 'ignore',
                                                 delimiter = ',', quotechar = '"')
            sentence_writer.writeheader() # write the column header to the output file

            row_count = 0 # original row count 72428 - check if row count changes
            for row in sentence_reader:
                #if re.search(r'^.*(include|from)r.*$', row['Text']):
                #if re.search(r'(include|from|part|some|-|with)', row['Text']):

                if re.search(r'\s(because|so that|therefore|as a result|problem|solution|since|cause|if it were not)\s', row['Text']): #regex specific to your sentence relationship
                    row_count = row_count + 1
                    row['CrowdFlower_Choices'] = row['Text'] #adds sentence to new column for future CrowdFlower task
                    sentence_writer.writerow(row) #writes each row to new csv output file

            print(row_count)

if __name__ == '__main__':
    main()