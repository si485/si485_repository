# README #

Description of Project:

For this project, we are helping to develop tools for summarizing data in a SQL database gathered by an online reading tutor for middle school students. Our overall goals are to add new annotations in the database, and take the existing dashboard prototype to make it better with those annotations and put on the web to update in real time. With this, we would also wish to extract features from the data to predict the progress of the students using the online tutor. Finally, we may want to work on an automated crowdsourcing pipeline for annotating and measuring the course content. With the success of this project, we wish to aid in teaching middle school kids literacy skills using technology. 